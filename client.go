package main

import (
	"fmt"
	"golang.org/x/net/websocket"
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"
	"time"
)

func getCPU() (idle, total uint64) {
	contents, err := ioutil.ReadFile("/proc/stat")
	if err != nil {
		return
	}

	lines := strings.Split(string(contents), "\n")
	for _, line := range lines {
		fields := strings.Fields(line)
		if fields[0] == "cpu" {
			numFields := len(fields)
			for i := 1; i < numFields; i++ {
				val, err := strconv.ParseUint(fields[i], 10, 64)
				if err != nil {
					fmt.Println("Error getting CPU usage...")
				}
				total += val
				if i == 4 {
					idle = val
				}
			}
			return
		}
	}
	return
}

func connect(ws *websocket.Conn) {
	var err error

	for {
		var reply string

		if err = websocket.Message.Receive(ws, &reply); err != nil {
			fmt.Println("Can't receive")
			break
		}

		idle0, total0 := getCPU()
		time.Sleep(1 * time.Second)
		idle1, total1 := getCPU()

		idleTicks := float64(idle1 - idle0)
		totalTicks := float64(total1 - total0)
		cpuUse := 100 * (totalTicks - idleTicks) / totalTicks
		msg := strconv.FormatFloat(cpuUse, 'f', 2, 64)

		if err = websocket.Message.Send(ws, msg); err != nil {
			fmt.Println("Can't send")
			break
		}
	}
}

func main() {
	http.Handle("/socket", websocket.Handler(connect))
	http.Handle("/", http.FileServer(http.Dir(".")))
	err := http.ListenAndServe(":8080", nil)
	if err != nil {
		panic("CRap 5")
	}
}
